#### Стек технологий:
* Flutter
* Android Studio 3+
* Dart
* MVP + Clean Architecture

Приложение отображает список рецептов, по нажатию на рецепт открывается подробное описание рецепта.

![Screenshot 1](https://bitbucket.org/gang018/cv_recipes_flutter/raw/0e3dc01b1b9ede77df7fc639561dce0a87e75a3f/screenshots/list.jpg)
![Screenshot 2](https://bitbucket.org/gang018/cv_recipes_flutter/raw/0e3dc01b1b9ede77df7fc639561dce0a87e75a3f/screenshots/card.jpg)