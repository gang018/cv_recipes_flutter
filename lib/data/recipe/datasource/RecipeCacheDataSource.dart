import 'package:cv_recipes/data/recipe/datasource/IRecipeDataSource.dart';
import 'package:cv_recipes/data/recipe/entity/Recipe.dart';

class RecipeCacheDataSource extends IRecipeDataSource {

  List<Recipe> recipes;

  @override
  Future<List<Recipe>> loadRecipes() async {
    return recipes;
  }

  @override
  Future<void> saveRecipes(final List<Recipe> recipes) async {
    print("saved to cache");
    return this.recipes = recipes;
  }

  @override
  Future<Recipe> getRecipe(String id) async {
    if (recipes == null) {
      return null;
    } else {
      return recipes.firstWhere((item) {
        return item.id == id;
      });
    }
  }
}