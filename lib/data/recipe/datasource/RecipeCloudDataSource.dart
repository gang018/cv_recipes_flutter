import 'package:cv_recipes/data/recipe/datasource/IRecipeDataSource.dart';
import 'package:cv_recipes/data/recipe/entity/Recipe.dart';
import 'package:cv_recipes/data/recipe/entity/RecipesListResponse.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'dart:io';

class RecipeCloudDataSource implements IRecipeDataSource {

  static const String RECIPES_BASE_URL = "http://food2fork.com/api/";
  static const String API_KEY = "026e11996a0cc33b052a7924e4f46757";

  static const String URL_SEARCH = RECIPES_BASE_URL + "search?key=" + API_KEY;

  static const Map<String, String> DEFAULT_HEADERS = {
    "Content-type": "application/json",
    "Request-Timeout": "10",
  };

  @override
  Future<List<Recipe>> loadRecipes() async {
    try {
      var response = await http.get(URL_SEARCH, headers: DEFAULT_HEADERS);
      if (response.statusCode == HttpStatus.ok) {
        return RecipesListResponse.fromJson(json.decode(response.body)).recipes;
      } else {
        return null;
      }
    } catch (error) {
      return null;
    }
  }

  @override
  Future<void> saveRecipes(final List<Recipe> recipes) {
    // ignore
  }

  @override
  Future<Recipe> getRecipe(String id) {
    // ignore
  }
}