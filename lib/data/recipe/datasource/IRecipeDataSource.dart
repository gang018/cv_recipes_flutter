import 'package:cv_recipes/data/recipe/entity/Recipe.dart';

abstract class IRecipeDataSource {

  Future<List<Recipe>> loadRecipes();

  Future<void> saveRecipes(final List<Recipe> recipes);

  Future<Recipe> getRecipe(String id);
}