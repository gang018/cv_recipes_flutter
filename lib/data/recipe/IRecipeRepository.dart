import 'package:cv_recipes/data/recipe/entity/Recipe.dart';

abstract class IRecipeRepository {

  Future<List<Recipe>> loadRecipes();

  Future<Recipe> getRecipe(String id);
}