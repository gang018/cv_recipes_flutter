import 'package:cv_recipes/data/recipe/IRecipeRepository.dart';
import 'package:cv_recipes/data/recipe/datasource/IRecipeDataSource.dart';
import 'package:cv_recipes/data/recipe/datasource/RecipeCacheDataSource.dart';
import 'package:cv_recipes/data/recipe/datasource/RecipeCloudDataSource.dart';
import 'package:cv_recipes/data/recipe/entity/Recipe.dart';

class RecipeRepository extends IRecipeRepository {

  final IRecipeDataSource localSource;

  RecipeRepository(): this.localSource = RecipeCacheDataSource();

  @override
  Future<List<Recipe>> loadRecipes() {
    return RecipeCloudDataSource().loadRecipes()
      .then((items) {
        localSource.saveRecipes(items);
        return items;
      });
  }

  @override
  Future<Recipe> getRecipe(String id) {
    return localSource.getRecipe(id);
  }
}