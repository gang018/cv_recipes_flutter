class Recipe {

  static const KEY_ID = "recipe_id";
  static const KEY_TITLE = "title";
  static const KEY_IMAGE_URL = "image_url";
  static const KEY_PUBLISHER = "publisher";
  static const KEY_PUBLISHER_URL = "publisher_url";
  static const KEY_SOCIAL_RANK = "social_rank";

  final String id;
  final String publisher;
  final String title;
  final String imageUrl;
  final double socialRank;
  final String publisherUrl;

  Recipe(String id, String name, String imageUrl, String publisher, String publisherUrl, double socialRank):
        this.id = id,
        this.title = name,
        this.imageUrl = imageUrl,
        this.publisher = publisher, this.publisherUrl = publisherUrl,
        this.socialRank = socialRank;

  factory Recipe.fromJson(Map<String, dynamic> json) {
    return Recipe(json[KEY_ID],
        json[KEY_TITLE],
        json[KEY_IMAGE_URL],
        json[KEY_PUBLISHER],
        json[KEY_PUBLISHER_URL],
        json[KEY_SOCIAL_RANK]
    );
  }
}