import 'package:cv_recipes/data/recipe/entity/Recipe.dart';

class RecipesListResponse {
  static const KEY_COUNT = "count";
  static const KEY_RECIPES = "recipes";

  final int count;
  final List<Recipe> recipes;

  RecipesListResponse(int count, List<Recipe> recipes):
        this.count = count, this.recipes = recipes;

  factory RecipesListResponse.fromJson(Map<String, dynamic> json) {
    final List<dynamic> recipes = json[KEY_RECIPES];
    return RecipesListResponse(
        json[KEY_COUNT],
        recipes.map((item) => Recipe.fromJson(item))
            .toList()
    );
  }
}