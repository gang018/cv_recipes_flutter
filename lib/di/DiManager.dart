import 'package:cv_recipes/data/recipe/IRecipeRepository.dart';
import 'package:cv_recipes/data/recipe/RecipeRepository.dart';
import 'package:cv_recipes/domain/RecipeInteractor.dart';
import 'package:cv_recipes/presentation/recipe/card/presenter/IRecipeCardPresenter.dart';
import 'package:cv_recipes/presentation/recipe/card/presenter/RecipeCardPresenter.dart';
import 'package:cv_recipes/presentation/recipe/list/presenter/IRecipesListPresenter.dart';
import 'package:cv_recipes/presentation/recipe/list/presenter/RecipesListPresenter.dart';

class DiManager {

  static IRecipeRepository repository;

  static IRecipesListPresenter getRecipeListPresenter() {
    return RecipesListPresenter(RecipeInteractor(getRepository()));
  }

  static IRecipeCardPresenter getRecipeCardPresenter() {
    return RecipeCardPresenter(RecipeInteractor(getRepository()));
  }

  static IRecipeRepository getRepository() {
    if (repository == null) {
      repository = RecipeRepository();
    }

    return repository;
  }
}