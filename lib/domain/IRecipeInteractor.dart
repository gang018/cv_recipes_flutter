import 'package:cv_recipes/data/recipe/entity/Recipe.dart';

abstract class IRecipeInteractor {
  Future<List<Recipe>> getRecipes();

  Future<Recipe> getRecipe(String id);
}