import 'package:cv_recipes/data/recipe/IRecipeRepository.dart';
import 'package:cv_recipes/data/recipe/entity/Recipe.dart';
import 'package:cv_recipes/domain/IRecipeInteractor.dart';

class RecipeInteractor implements IRecipeInteractor {

  final IRecipeRepository repository;

  RecipeInteractor(IRecipeRepository repo): this.repository = repo;

  @override
  Future<List<Recipe>> getRecipes() {
    return repository.loadRecipes();
  }

  @override
  Future<Recipe> getRecipe(String id) {
    return repository.getRecipe(id);
  }
}