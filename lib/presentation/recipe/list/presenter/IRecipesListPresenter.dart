import 'package:cv_recipes/data/recipe/entity/Recipe.dart';
import 'package:cv_recipes/presentation/recipe/list/view/IRecipesListView.dart';

abstract class IRecipesListPresenter {

  void handleViewLoaded(IRecipesListView view);

  void handleRefreshClick();

  void handleItemClicked(Recipe recipe);

  void dispose();
}