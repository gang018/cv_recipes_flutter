import 'package:cv_recipes/data/recipe/entity/Recipe.dart';
import 'package:cv_recipes/domain/IRecipeInteractor.dart';
import 'package:cv_recipes/presentation/recipe/list/presenter/IRecipesListPresenter.dart';
import 'package:cv_recipes/presentation/recipe/list/view/IRecipesListView.dart';

class RecipesListPresenter implements IRecipesListPresenter {

  IRecipesListView _view;
  final IRecipeInteractor _interactor;

  RecipesListPresenter(IRecipeInteractor interactor): this._interactor = interactor;

  @override
  void handleViewLoaded(IRecipesListView view) {
    if (this._view == null) {
      this._view = view;

      _loadRecipes();
    }
  }

  void _loadRecipes() {
    _view.showState(IRecipesListView.STATE_LOADING);
    _interactor.getRecipes()
        .then((list) {
          print("items are = $list");

          if (list == null || list.isEmpty) {
            _view.showState(IRecipesListView.STATE_ERROR);
          } else {
            _view.setData(list);
            _view.showState(IRecipesListView.STATE_CONTENT);
          }
        });
  }

  @override
  void handleRefreshClick() {
    _loadRecipes();
  }


  @override
  void handleItemClicked(Recipe recipe) {
    if (_view != null) {
      _view.routeToRecipeCard(recipe);
    }
  }

  @override
  void dispose() {
    _view = null;
  }
}