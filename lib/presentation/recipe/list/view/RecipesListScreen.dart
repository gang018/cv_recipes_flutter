import 'package:cv_recipes/presentation/recipe/list/view/RecipesListState.dart';
import 'package:flutter/widgets.dart';

class RecipesListScreen extends StatefulWidget {

  @override
  State<StatefulWidget> createState() {
    return RecipesListState();
  }
}