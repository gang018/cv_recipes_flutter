import 'package:cv_recipes/data/recipe/entity/Recipe.dart';
import 'package:cv_recipes/presentation/recipe/list/view/IRecipesListView.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class RecipesListViewBuilder {

  static const Key KEY = Key("RecipesListWidget");
  static const double LOADER_SIZE = 50;
  static const double FONT_SIZE_18 = 18;
  static const double FONT_SIZE_16 = 16;
  static const double ERROR_TEXT_MARGIN = 5;

  Widget _loader;
  Widget _errorView;
  ListView _contentView;
  List<Recipe> _data;

  RecipesListViewBuilder();

  Widget buildView(int state, void refreshClick(), void itemClickListener(Recipe recipe)) {
    Widget newChild;
    switch (state) {
      case IRecipesListView.STATE_LOADING: {
        newChild = _getLoader();
        break;
      }

      case IRecipesListView.STATE_ERROR: {
        newChild = _getError(refreshClick);
        break;
      }

      case IRecipesListView.STATE_CONTENT: {
        newChild = _getContent(itemClickListener);
        break;
      }

      default: {
        newChild = _getError(refreshClick);
        break;
      }
    }

    return Scaffold(
      appBar: AppBar(
        title: Text("Recipes list")),
      body: Center(
        child: newChild,
      ),
    );
  }

  Widget _getLoader() {
    if (_loader == null) {
      _loader = Container(
          alignment: AlignmentDirectional.center,
          width: LOADER_SIZE,
          height: LOADER_SIZE,
          child: CircularProgressIndicator()
      );
    }

    return _loader;
  }

  Widget _getError(void refreshClick()) {
    if (_errorView == null) {
      _errorView = Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Text(
            "Ooops! Something is wrong.\nPlease, try again",
            textAlign: TextAlign.center,
            style: TextStyle(
                color: Colors.black,
                fontSize: FONT_SIZE_18,
                fontWeight: FontWeight.w200
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: ERROR_TEXT_MARGIN),
            child: MaterialButton(
                onPressed: refreshClick,
                color: Colors.green,
                child: Text(
                  "TRY AGAIN",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      fontSize: FONT_SIZE_16,
                      color: Colors.white
                  ),
                )
            )
          )
        ],
      );
    }

    return _errorView;
  }

  Widget _getContent(void itemClickListener(Recipe recipe)) {
    if (_contentView == null) {
      _contentView = ListView.separated(
        itemBuilder: (context, index) {
          final Recipe item = _data[index];
          return ListTile(
            title: Text(item.title),
            subtitle: Text("#" + item.publisher),
            onTap: () => itemClickListener(item)
          );
        },
        separatorBuilder: (context, index) {
          return Divider();
        },
        itemCount: _data.length
      );
    }

    return _contentView;
  }

  void setData(List<Recipe> data) {
    _data = data;
  }
}