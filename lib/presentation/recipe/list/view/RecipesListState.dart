import 'package:cv_recipes/data/recipe/entity/Recipe.dart';
import 'package:cv_recipes/di/DiManager.dart';
import 'package:cv_recipes/presentation/recipe/card/view/RecipeCardScreen.dart';
import 'package:cv_recipes/presentation/recipe/list/presenter/IRecipesListPresenter.dart';
import 'package:cv_recipes/presentation/recipe/list/view/IRecipesListView.dart';
import 'package:cv_recipes/presentation/recipe/list/view/RecipesListScreen.dart';
import 'package:cv_recipes/presentation/recipe/list/view/RecipesListViewBuilder.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter/material.dart';

class RecipesListState extends State<RecipesListScreen> implements IRecipesListView {

  int _state;
  RecipesListViewBuilder _builder;
  IRecipesListPresenter _presenter;

  @override
  void initState() {
    super.initState();

    this._presenter = DiManager.getRecipeListPresenter();
  }

  @override
  Widget build(BuildContext context) {
    if (this._builder == null) {
      this._builder = RecipesListViewBuilder();
    }

    WidgetsBinding.instance.addPostFrameCallback((_) {
      if (_presenter != null) {
        _presenter.handleViewLoaded(this);
      }
    });

    return this._builder.buildView(_state, () {
      if (_presenter != null) {
        _presenter.handleRefreshClick();
      }},
      (recipe) {
        _presenter.handleItemClicked(recipe);
      }
    );
  }

  @override
  void showState(int state) {
    setState(() {
      _state = state;
    });
  }

  @override
  void setData(List<Recipe> recipes) {
    print("setData");
    if (_builder != null) {
      this._builder.setData(recipes);
    }
  }


  @override
  void routeToRecipeCard(Recipe recipe) {
    Navigator.push(context, MaterialPageRoute(builder: (context) => RecipeCardScreen(recipe)));
  }

  @override
  void dispose() {
    super.dispose();

    this._builder = null;
    _presenter.dispose();
    _presenter = null;
  }
}