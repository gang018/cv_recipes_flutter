import 'package:cv_recipes/data/recipe/entity/Recipe.dart';

abstract class IRecipesListView {

  static const int STATE_LOADING = 0;
  static const int STATE_ERROR = STATE_LOADING + 1;
  static const int STATE_CONTENT = STATE_ERROR + 1;

  void showState(int state);

  void setData(List<Recipe> recipes);

  void routeToRecipeCard(Recipe recipe);
}