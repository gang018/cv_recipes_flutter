import 'package:cv_recipes/data/recipe/entity/Recipe.dart';
import 'package:cv_recipes/domain/IRecipeInteractor.dart';
import 'package:cv_recipes/presentation/recipe/card/presenter/IRecipeCardPresenter.dart';
import 'package:cv_recipes/presentation/recipe/card/view/IRecipeCardView.dart';

class RecipeCardPresenter implements IRecipeCardPresenter {

  final IRecipeInteractor interactor;
  IRecipeCardView _view;
  Recipe _recipe;

  RecipeCardPresenter(IRecipeInteractor interactor): this.interactor = interactor;


  @override
  void handleViewLoad(IRecipeCardView view, Recipe recipe) {
    if (_recipe == null) {
      _recipe = recipe;
      _view = view;
    }
  }

  @override
  void handleRouteToSiteClick() {
    if (_view != null) {
      _view.routeToAuthorsPage(_recipe.publisherUrl);
    }
  }

  @override
  Future<Recipe> getRecipe(String id) {
    return interactor.getRecipe(id);
  }
}