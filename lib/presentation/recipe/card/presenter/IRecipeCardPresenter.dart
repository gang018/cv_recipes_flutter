import 'package:cv_recipes/data/recipe/entity/Recipe.dart';
import 'package:cv_recipes/presentation/recipe/card/view/IRecipeCardView.dart';

abstract class IRecipeCardPresenter {

  void handleViewLoad(IRecipeCardView view, Recipe recipe);

  void handleRouteToSiteClick();
}