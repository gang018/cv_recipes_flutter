import 'package:cv_recipes/data/recipe/entity/Recipe.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class RecipeCardViewBuilder {

  static const double MARGIN_TITLE = 20;
  static const double ZERO = 0;
  static const double FONT_SIZE_18 = 18;
  static const double IMAGE_HEIGHT = 250;

  Widget build(BuildContext context, Recipe recipe, void recipeClickListener()) {
    return Scaffold(
      appBar: AppBar(
        title: Text(recipe.title),
      ),
      body: _getContent(context, recipe, recipeClickListener),
    );
  }

  Widget _getContent(BuildContext context, Recipe recipe, void recipeClickListener()) {
    return Column(
      children: <Widget>[
        Image.network(
          recipe.imageUrl,
          height: IMAGE_HEIGHT,
          width: MediaQuery.of(context).size.width,
          fit: BoxFit.cover,
        ),
        Container(
          margin: EdgeInsets.only(bottom: ZERO, left: MARGIN_TITLE, right: MARGIN_TITLE, top: MARGIN_TITLE),
          alignment: Alignment.centerLeft,
          child: Text(
            recipe.title,
            style: TextStyle(
              color: Colors.black87,
              fontWeight: FontWeight.w600,
              fontSize: FONT_SIZE_18,
            ),
          ),
        ),
        Container(
          margin: EdgeInsets.only(bottom: MARGIN_TITLE, left: MARGIN_TITLE, right: MARGIN_TITLE, top: ZERO),
          alignment: Alignment.centerLeft,
          child: Text(
            "#" + recipe.publisher,
            style: TextStyle(
              color: Colors.black87,
              fontWeight: FontWeight.w200,
              fontSize: FONT_SIZE_18,
            ),
          ),
        ),
        MaterialButton(onPressed: recipeClickListener,
          color: Colors.green,
          child: Text(
            "OPEN AUTHOR'S PAGE",
            style: TextStyle(
              color: Colors.white,
              fontSize: FONT_SIZE_18
            ),
          ),
        )
      ],
    );
  }
}