import 'package:cv_recipes/data/recipe/entity/Recipe.dart';
import 'package:cv_recipes/presentation/recipe/card/view/RecipeCardState.dart';
import 'package:flutter/widgets.dart';

class RecipeCardScreen extends StatefulWidget {

  final Recipe _recipe;

  RecipeCardScreen(Recipe recipe): this._recipe = recipe;

  @override
  State<StatefulWidget> createState() => RecipeCardState(_recipe);
}