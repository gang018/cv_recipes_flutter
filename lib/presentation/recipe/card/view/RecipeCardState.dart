import 'package:cv_recipes/data/recipe/entity/Recipe.dart';
import 'package:cv_recipes/di/DiManager.dart';
import 'package:cv_recipes/presentation/recipe/card/presenter/IRecipeCardPresenter.dart';
import 'package:cv_recipes/presentation/recipe/card/view/IRecipeCardView.dart';
import 'package:cv_recipes/presentation/recipe/card/view/RecipeCardScreen.dart';
import 'package:cv_recipes/presentation/recipe/card/view/RecipeCardViewBuilder.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:url_launcher/url_launcher.dart';

class RecipeCardState extends State<RecipeCardScreen> implements IRecipeCardView {

  final Recipe _recipe;
  IRecipeCardPresenter _presenter;
  final RecipeCardViewBuilder _builder;

  RecipeCardState(Recipe recipe): this._recipe = recipe, _builder = RecipeCardViewBuilder();

  @override
  Widget build(BuildContext context) {
    _presenter = DiManager.getRecipeCardPresenter();

    WidgetsBinding.instance.addPostFrameCallback((_) {
      if (_presenter != null) {
        _presenter.handleViewLoad(this, _recipe);
      }
    });

    return _builder.build(context, _recipe, () {
      if (_presenter != null) {
        _presenter.handleRouteToSiteClick();
      }
    });
  }

  @override
  void routeToAuthorsPage(String page) {
    canLaunch(page)
        .then((can) {
          if (can) {
            launch(page);
          }
        });
  }
}